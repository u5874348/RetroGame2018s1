package com.example.zzz.a2100;

import android.graphics.Canvas;
import android.graphics.Paint;

// by Ziqi Zhang and Zizhao Zhang
public class ball extends obj {

        public static final float initial_radius =0.02f;
        public static final float STARTX = 0.5f;
        public static final float STARTY = 0.8f;
        public static final float  initial_Xspeed =0.003f;
        public static final float  initial_Yspeed = 0.01f;
        public  float Xspeed;
        public  float Yspeed;
        public  float radius;
        public  boolean cross=false;                //
        public float drawR;
        boolean die;

        public ball() {
        this.pos = new Pos(STARTX,STARTY);
        this.Xspeed=initial_Xspeed;
        this.Yspeed=initial_Yspeed;
        this.radius=initial_radius;
        this.die=false;
        drawR=initial_radius;
        }

        public ball(Pos pos) {
        this.pos = pos;
        this.Xspeed=initial_Xspeed;
        this.Yspeed=initial_Yspeed;
        this.radius=initial_radius;
        drawR=initial_radius;
        this.die=false;
        }

        @Override
        public void draw(Canvas c , Paint p) {
            int h = c.getHeight();
            int w = c.getWidth();
            float xball = this.pos.x * w;
            float yball = this.pos.y * h;
            float r = w * drawR;
            c.drawCircle(xball, yball, r, p);
            }

         public void reverseAll()
         {
             this.Xspeed=(-1)*this.Xspeed;
             this.Yspeed=(-1)*this.Yspeed;
         }

         public void reverseX()
          { this.Xspeed=(-1)*this.Xspeed; }


          public void reverseY()
           { this.Yspeed=(-1)*this.Yspeed; }


         public void speedMul(float muiltipler)
         { this.Xspeed=muiltipler*this.Xspeed; }



         public void  sizeMul(float muiltipler)
         {
             this.drawR=muiltipler * initial_radius;
         }


         public boolean  if_hitWall()
         {   if((this.pos.x-radius<= game.leftWall) ||(this.pos.x+radius>=game.rightWall)) {return true;}
             if((this.pos.y-radius<=game.upperWall)) {return true;}
             return false;
         }


         // return int telling which wall the ball just hit
         // 0 is default,
         // 1 for left ,
         // 2 for top-left,
         // 3 for top,
         // 4 for top-right,
         // 5 for right,
         // 6 is falling

          public int hit_which_Wall() {

            if(this.pos.x-radius<= game.leftWall)
           {return  1;}
           else if(this.pos.y-radius<=game.upperWall)
           {return 3;}
           else if(this.pos.x+radius>=game.rightWall)
           {return 5;}
           if (this.pos.y+radius>=game.lowwerWall){return 6;};
           return 0;
           }





         public float x_Y_speed(float xspeed,float yspeed)
         { return (float) Math.sqrt(xspeed*xspeed+yspeed*yspeed); }



}





