package com.example.zzz.a2100;
public class Pos {//help to calculate the position when the ball hit the brick/pane/wall.
    float x;
    float y;
    public Pos(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public Pos(Pos p) {
        this.x = p.x;
        this.y = p.y;
    }

    public float Eculideandistance (Pos p) {
        float dx = x - p.x;
        float dy = y - p.y;
        return (float) Math.sqrt(dx*dx + dy*dy);
    }
// absolute distance in x diemnsion
    public float b_x_distance (Pos p) {
        float dx = x - p.x;
        return Math.abs(dx);
    }
// absolute distance in y dimension
    public float b_y_distance (Pos p) {
        float dy = y - p.y;
        return Math.abs(dy);
    }

}
