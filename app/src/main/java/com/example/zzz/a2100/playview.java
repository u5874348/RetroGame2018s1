package com.example.zzz.a2100;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// by Ziqi Zhang and Zizhao Zhang

public class playview extends View implements View.OnTouchListener ,  Runnable,GameOver {
    public static final int STEPDELAY = 100;
    public static final int DEFAULTCOLOUR = Color.BLACK;
    public static final float DEFAULTPENWIDTH = 3.0f;
    Bitmap pict = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.timeup);
    Bitmap pict1 = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.timedown);
    Bitmap pict2 = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.lifeup);
    Bitmap pict3 = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.lifedown);



    //default settings.
    Paint bspaint,brickpaint,panepaint,line;
    Handler repaintHandler;
    game game;
    static int life;
    static int score=0;
    static int time=75;
    int cal=0;
    static int level=1;
    String text;
    String text1;
    ArrayList<GameOver> observers;
    int diffculty;

    public playview(Context context, AttributeSet attrs) {
        super(context, attrs);

        life=Main4Activity.life;
        diffculty= Main4Activity.difficult;

       if (life==0) {//if life=0 the game is over.The next game is the default setting in mainactivitity1.
           life = MainActivity.life2;
           diffculty = MainActivity.difficult2;
       }

        panepaint=new Paint();
        panepaint.setColor(DEFAULTCOLOUR);//paint the pane.

        bspaint = new Paint();
        bspaint.setColor(DEFAULTCOLOUR);//paint the balls.

        brickpaint = new Paint();




        this.setOnTouchListener(this);
        observers = new ArrayList<GameOver>();
        game = new game(diffculty);

        repaintHandler = new Handler();
        repaintHandler.postDelayed(this, STEPDELAY);
        this.registerGameOver(this);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float w = (float) v.getWidth();
        game.touch(event.getX() / w);
        return true;
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        line=new Paint();
        line.setColor(DEFAULTCOLOUR);
        line.setStrokeWidth(5f);
        canvas.drawLine(0,100f,1200f,100f,line);//setting the text(level,score,life) above the line.
        text=life+"";
        text1=score+"";
        String text2=level+"";
        String timetext=time+"";
        Paint textp=new Paint();
        textp.setTextSize(75f);
        Paint scorep=new Paint();
        scorep.setTextSize(75f);
        Paint timep=new Paint();
        if (time<=30) {scorep.setColor(Color.RED);timep.setColor(Color.RED);}//when time is less than 30s,the score will double and the color will be red to remind you.



        canvas.drawText(text,500f,100f,textp);//draw the text.
        canvas.drawText(text1,900f,100f,scorep);
        canvas.drawText(text2,200f,100f,textp);

        timep.setTextSize(500f);
        canvas.drawText(timetext,0.2f*this.getWidth(),0.5f*this.getHeight(),timep);

        game.draw(canvas, bspaint,brickpaint,panepaint);






    }

    public boolean step()  {

        if (Math.random()<0.015){//the percentage that items appear. Randomly appears in the game.
            Random rm=new Random();//generate 4 kinds of items.
            int i=rm.nextInt(6);
            if (i==0|i==1) game.itemadd(1,pict);
            else if(i==2|i==3) game.itemadd(2,pict1);
            else if(i==4) game.itemadd(3,pict2);
            else if(i==5) game.itemadd(4,pict3);

        }


        if ((game.lose||game.haslost())&&(life>0))
        {game.IMCOMINGBACK();game.lose=false;


        }
        if (cal==10) {time-=1;cal=0;}else {cal+=1;}

        //100ms is 1s.
        game.step();

        if (game.hasWon()){//go to the next level.level+1. Time=75. Show the level up toast.
           game=new game(diffculty);
           this.time=75;
           this.level+=1;
            Context context =this.getContext();
            CharSequence text = "Level up!";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }


        if(life==0||time==0 ){//game over.
            notifyGameOver();
            return false;//tell the function do not step.
        }
        if (  game.lose||game.haslost()) {
            life-=1;
            Context context =this.getContext();
            CharSequence text = "    LIFE-1"+"\n"+" ( remaining: "+life+")";//when all the ball is falled,life-1. Show the toast to remind you.
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


        }
        this.invalidate();//update.
        return true;
    }
    private void notifyGameOver() {
        for (GameOver o : observers) o.gameOver();
    }

    @Override
    public void run() {
        if (step()) {
            repaintHandler.postDelayed(this, playview.STEPDELAY);
        }
    }

    public void registerGameOver(GameOver gameover) {
        observers.add(gameover);
    }

    @Override
    public void gameOver() {
        Context context =this.getContext();
        CharSequence text = "GAME OVER!!";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        this.getContext().startActivity(new Intent(this.getContext(),Main3Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );

    }
}
