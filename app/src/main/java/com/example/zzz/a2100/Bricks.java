package com.example.zzz.a2100;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import static com.example.zzz.a2100.playview.DEFAULTPENWIDTH;
import static com.example.zzz.a2100.playview.level;
// by Ziqi Zhang and Zizhao Zhang
public class Bricks extends ArrayList<Brick> {


   public static Bricks gridBricks (int rows){
       Bricks res=new Bricks();
       float begin_y =game.upperWall+0.08f;
       float begin_x =game.leftWall+2.5f*SimpleBrick.xLength;
       int cols=(int)(((game.rightWall-game.leftWall)/(2*SimpleBrick.xLength))-2);



       for(int i=0;i<rows;i++){//generate basic brisk.
           for(int j =0;j<cols;j++)
           {    Brick basic=new SimpleBrick(new Pos(begin_x+j*(2*SimpleBrick.xLength+ball.initial_radius),begin_y+i*(2*SimpleBrick.yLength+ball.initial_radius)));

               Random rm=new Random();
               int decorate=rm.nextInt(3);//1/3 of the brisk may become the colorful one.
               if (decorate==1){
               int kk=rm.nextInt(2);
               float kl=rm.nextFloat() *2;
               if((i+j)%(kk+1)==0){
                   brickDecorator basic2= new speedBrick(kl,basic);//the ball generate by color brisk may become fast or slow.
                   basic=basic2;
                    }
               int kk3=rm.nextInt(5);
               float kl3=rm.nextFloat()/2+1;
               if((i+j)%(kk3+1)==0){
                   brickDecorator basic2= new getBiggerBrick(kl3,basic);//may become bigger or smaller.
                   basic=basic2;}

               brickDecorator basic2= new splitBrick(1,basic);//all colorful brisks will generate a new ball.
               basic=basic2;}

                res.add(basic);
           }
       }
       return res;

   }// by Ziqi Zhang and Zizhao Zhang
    public void draw(Canvas canvas, Paint paint) {

        for (Brick a : this) {
            if (a instanceof SimpleBrick){//paint the simple brick.
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(DEFAULTPENWIDTH);
            }
            else
            {
                paint.setStyle(Paint.Style.FILL);//paint colorful brick.use setARGB().3 random numbers between 0-255 means the percentage of red yellow and blue.
            int[] b=getRandomcolor();
            paint.setARGB(255,b[0],b[1],b[2]);
            }
            a.draw(canvas, paint);
        }

    }

// by Ziqi Zhang and Zizhao Zhang
    public void step() {

    }
    int[] getRandomcolor(){// generate 3 random int between 0-255.
        Random rand =new Random();
        int i1=rand.nextInt(256);
        int i2=rand.nextInt(256);
        int i3=rand.nextInt(256);
        int a[]={i1,i2,i3};
        return a;
    }
// by Ziqi Zhang and Zizhao Zhang
    public void removeHit(balls bs) {
        Iterator<Brick> ai = this.iterator();
        while (ai.hasNext()) {
            Brick a = ai.next();
            if (a.hitted(bs)) {
                a.hitaction(bs);
                ai.remove();
                if (playview.life<=30) playview.score+=playview.level*2;
                else playview.score+=level;

            }
        } }

}
