package com.example.zzz.a2100;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences share2;
    private SharedPreferences.Editor editor2;
    private static final String filename="peizhi";

    static int difficult2;
    static int life2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = (Button) findViewById(R.id.button);
        Button b1=(Button)findViewById(R.id.button2);
        Button b2=(Button)findViewById(R.id.button3);
        b.setOnClickListener(this);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);


        share2=this.getSharedPreferences(filename, Activity.MODE_PRIVATE);
        editor2=share2.edit();
        this.difficult2=share2.getInt("diff",3);//get default settings.
        this.life2=share2.getInt("life",3);
    }



    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.button)
                { Intent intent = new Intent(this, Main2Activity.class);
                  playview.level=1;//start a new game. set all to default value.
                  playview.score=0;
                  playview.time=75;
                  startActivity(intent);
                }
                else if (v.getId()==R.id.button2)//to options
                    {
                        Intent intent = new Intent(this, Main4Activity.class);
                        startActivity(intent);
                    }
                    else{Intent intent = new Intent(this, Main5Activity.class);
                          startActivity(intent);//to how to play

        }


    }
}
