package com.example.zzz.a2100;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.Toast;

import java.util.Random;
// by Ziqi Zhang and Zizhao Zhang
public class Movingpane extends obj {

    public static float xLength = 0.1f;
    public static float yLength = 0.0000001f;
    public static float ini_p_x  = 0.5f;
    public static float ini_p_y  = 0.997f;
    public float speedPane;
    public int leftTouch;//1 is left ,2 is right, 0 is middle

    public Movingpane(){
        this.pos=new Pos(ini_p_x,ini_p_y);
        this.speedPane=0;
        this.leftTouch=0;
    }

    public void paneHITitem(Items items){//the pane picked the items.

        for (Item i:items){
            if (i.x+0.24f>=this.pos.x & i.x<=this.pos.x+xLength & i.y+0.10f>=ini_p_y&i.y<=0.95f){
                i.die=true;
                playview.score+=50;//get score.
                if (i.i==1) playview.time+=10;//item.i is the index of the item.Item 1 is time+10.
                else if (i.i==2) {playview.time-=10;playview.score-=100;}//this is time-10 and score-50.
                else if (i.i==3) {playview.life+=1;}//life+1.
                else if (i.i==4) {playview.life-=1;playview.score-=100;}//life-1 score-50


            }
        }
    }

    public void paneHITball(balls bs){//gives a slight random speed to the pane,which makes the ball will not rebound vertically.
        Random rm=new Random();
        float xx=rm.nextFloat()/1300;
        if (leftTouch==0)
        {this.speedPane=xx;}
        else if (leftTouch==1){this.speedPane=0.05f;
        }else{this.speedPane=-0.05f;}



        for(ball b:bs){
            if ((b.pos.b_x_distance(this.pos) <= b.radius + xLength + game.redundency)
                    && (b.pos.b_y_distance(this.pos) <= b.radius + yLength + game.redundency)){
                b.reverseY();//the ball hit the moving pane. reverse.


                if((speedPane>0 && b.Xspeed>=0)||(speedPane<0 && b.Xspeed<=0)){
                  b.Xspeed-=this.speedPane;//gives the ball an extra speed of the pane.
                }
                  else{
                    b.reverseX();
                    b.Xspeed+=this.speedPane;
                }
            }

        }

    }






    @Override
    public void draw(Canvas c, Paint p) {//default position
        int h = c.getHeight();
        int w = c.getWidth();
        float left = (this.pos.x -xLength)*w ;
        float top = (this.pos.y-160000*yLength)*h;
        float right = (this.pos.x+xLength)*w;
        float bottom = (this.pos.y+15000*yLength)*h;

        c.drawRect(left,top, right,  bottom, p);
    }
}
