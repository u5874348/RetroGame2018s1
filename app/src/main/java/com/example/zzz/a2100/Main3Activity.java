package com.example.zzz.a2100;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Button b1=(Button)findViewById(R.id.button4);
        b1.setOnClickListener(this);
        TextView t=(TextView)findViewById(R.id.textView);
        t.setText(playview.score+""); //At the gameover view, show the score.
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.button4)
        { Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent); //the button back jump to the main page.
        }



    }

}
