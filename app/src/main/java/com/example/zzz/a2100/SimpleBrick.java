package com.example.zzz.a2100;


import android.graphics.Canvas;
import android.graphics.Paint;



// The most simple brick to be decorated 
public class SimpleBrick extends  Brick {

    public static float  xLength = 0.05f;
    public static float yLength = 0.01f;


    public SimpleBrick(Pos pos) {
        this.pos = pos;
        this.hitB=false;
    }

// return true if the brcik is hitted 
    public boolean hitted(balls bs) {
        for (ball b : bs) {
            if (((b.pos.b_x_distance(this.pos) <=   xLength + game.redundency) && (b.pos.b_y_distance(this.pos) <= b.radius+yLength + game.redundency)
                    ) ||((b.pos.b_x_distance(this.pos) <= b.radius+  xLength + game.redundency) && (b.pos.b_y_distance(this.pos) <= yLength + game.redundency))
                    ) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void draw(Canvas c, Paint p) {
        int h = c.getHeight();
        int w = c.getWidth();
        float left = (this.pos.x -xLength)*w ;
        float top = (this.pos.y-yLength)*h;
        float right = (this.pos.x+xLength)*w;
        float bottom = (this.pos.y+yLength)*h;

        c.drawRect(left,top, right,  bottom, p);

    }
// the most simple hit action is just reverse the ball
    @Override
    public void hitaction(balls bs) {
        for(ball b:bs){
            if (((b.pos.b_x_distance(this.pos) <=   xLength + game.redundency) && (b.pos.b_y_distance(this.pos) <= b.radius+yLength + game.redundency)
            ) ||((b.pos.b_x_distance(this.pos) <= b.radius+  xLength + game.redundency) && (b.pos.b_y_distance(this.pos) <= yLength + game.redundency))
                    )
                            {
                if(b.pos.b_x_distance(this.pos)<=xLength-game.redundency)
                { b.reverseY(); }
                else if(b.pos.b_y_distance(this.pos)<=yLength-game.redundency)
                {b.reverseX();}
                else{b.reverseAll(); }

            }
        }






    }
}