package com.example.zzz.a2100;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
// by Ziqi Zhang and Zizhao Zhang
public class Item extends obj  {
    float y;
    float x;
    int i;
    Bitmap b;
    boolean die=false;
    public Item(int i,Bitmap b){
        float rand=(float)Math.random();
        this.x=rand;//new item generates at random x position.
        this.y=0.01f;
        this.i=i;//the index of the item. help to get the function.
        this.b=b;//the picture of the item.

    }
// by Ziqi Zhang and Zizhao Zhang
    @Override
    public void draw(Canvas c,Paint paint) {

        int h=c.getHeight();//relevant position.
        int w=c.getWidth();



        c.drawBitmap(b,x*w,y*h,new Paint());


    }
}
