package com.example.zzz.a2100;
import android.graphics.Canvas;
import android.graphics.Paint;


// by Ziqi Zhang and Zizhao Zhang

public abstract  class Brick extends obj {
    float xLength;
    float yLength;
    boolean hitB;

    public abstract boolean hitted(balls bs);//whether it hitted by ball.
    public abstract void draw(Canvas c, Paint p);//draw the brick.
    public abstract void hitaction(balls bs);//when hit, what to do.
}
