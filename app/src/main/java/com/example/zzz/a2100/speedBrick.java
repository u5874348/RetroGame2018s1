package com.example.zzz.a2100;
import android.graphics.Canvas;
import android.graphics.Paint;

// by Ziqi Zhang and Zizhao Zhang
public class speedBrick extends brickDecorator{

float speedchange;
public speedBrick(float speedchange,Brick m2brick){
    this.brick=m2brick;
    this.pos=new Pos(brick.pos.x,brick.pos.y);
    this.speedchange=speedchange;

    };

    public void hitaction(balls bs){
        brick.hitaction(bs);
        for(ball b:bs){
            float dx=brick.xLength;
            float dy=brick.yLength;
            if (b.pos.Eculideandistance(this.pos)<game.redundency+Math.sqrt(dx*dx+dy*dy))
            {b.speedMul(speedchange);}
        }
    };


    @Override
    public boolean hitted(balls bs) {

        return brick.hitted( bs);
    }

    @Override
    public void draw(Canvas c, Paint p) {
   this.brick.draw( c,p);
    }
}
