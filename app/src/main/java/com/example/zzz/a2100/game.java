package com.example.zzz.a2100;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Iterator;
import java.util.Random;

// by Ziqi Zhang and Zizhao Zhang
public class game {
    public balls bs;
    public Movingpane movingpane;
    public Bricks bricks;
    public Items Items;
    public boolean lose;
    public boolean win;


  public static  int ini_rows_of_bricks=16;
    public int rows_of_bricks;
    public static  float leftWall= 0.001f;
    public static float rightWall= 0.99f;
    public static float upperWall= 0.0000001f;
    public static float lowwerWall= 0.99999999999999999999f;
    public static  float redundency=0.00f;

// by Ziqi Zhang and Zizhao Zhang
    public game(int difficulty)
    {

       if (difficulty==0){rows_of_bricks=1;}
        else if (difficulty==1){rows_of_bricks=(int)(ini_rows_of_bricks/3f);}
        else if (difficulty==2){rows_of_bricks=(int)(ini_rows_of_bricks/2f);}
       else if (difficulty==3){rows_of_bricks=(int)(ini_rows_of_bricks/1.5f);}
       else if (difficulty==4){rows_of_bricks=(int)(ini_rows_of_bricks/1.1f);}
        else {rows_of_bricks=ini_rows_of_bricks+1;}

        bs=balls.getTheFirstBallsList();
        bricks=Bricks.gridBricks(rows_of_bricks);
        Items=new Items();
    movingpane=new Movingpane();
    win=false;lose=false;
    }


// by Ziqi Zhang and Zizhao Zhang
    public void draw(Canvas canvas, Paint bspaint,Paint brickpaint,Paint panepaint) {
        bs.draw(canvas, bspaint);
        bricks.draw(canvas, brickpaint);
        movingpane.draw(canvas, panepaint);
        Items.draw(canvas,new Paint());
    }


// by Ziqi Zhang and Zizhao Zhang
public void IMCOMINGBACK(){
        this.bs.add(new ball());
    this.bs.add(new ball());
    }

// by Ziqi Zhang and Zizhao Zhang
    public void itemadd(int i, Bitmap b){
        Items.add(new Item(i,b));
    }

// by Ziqi Zhang and Zizhao Zhang
    public void step() {//each step(100ms), all the actions of bricks,balls and moving pane.

        bs.step(this);
        bricks.step();
        Items.step();
        bricks.removeHit(bs);//check all brisks which has been hit.
        movingpane.paneHITball(bs);//if pane hit ball,do something.
        movingpane.paneHITitem(Items);//if pane collected items, do something.
        Iterator<ball> ai = bs.iterator();//use iterator to remove ball.
        while (ai.hasNext()) {
           ball a = ai.next();
            if (a.die) {
                ai.remove(); }
        }
        Iterator<Item> it = Items.iterator();//use iterator to remove ball.
        while (it.hasNext()) {
            Item b = it.next();
            if (b.die) {
                it.remove(); }
        }

        movingpane.speedPane=0;



    }
    public boolean hasWon() {

        return bricks.size()==0;
    }

    public boolean haslost() {

        return bs.size()==0;
    }
// by Ziqi Zhang and Zizhao Zhang
    public void touch(float xpos) {
        if (xpos<movingpane.pos.x){movingpane.leftTouch=1;}
        else if(xpos>movingpane.pos.x){movingpane.leftTouch=2;}
        movingpane.pos.x = xpos;
// 1 is left ,2 is right,0 is middle
    }



}
