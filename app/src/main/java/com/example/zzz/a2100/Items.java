package com.example.zzz.a2100;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
// by Ziqi Zhang and Zizhao Zhang
public class Items extends ArrayList<Item> {//the list of item.
    float y;//x,y position
    float x;
    int i;//index of item.
    Bitmap b;//the bitmap.
// by Ziqi Zhang and Zizhao Zhang
    public void step(){//each step the item falls in this speed.
      for (Item i:this){
          i.y+=0.01f;
      }
    }// by Ziqi Zhang and Zizhao Zhang
    public void draw(Canvas canvas, Paint paint){//draw each item.

        for(Item c:this){
            c.draw(canvas,paint);
        }
    }
}
