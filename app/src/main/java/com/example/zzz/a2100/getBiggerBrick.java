package com.example.zzz.a2100;
import android.graphics.Canvas;
import android.graphics.Paint;

// by Ziqi Zhang and Zizhao Zhang

public class getBiggerBrick extends brickDecorator {
    float radiuschange;


    public getBiggerBrick(float radiuschange,Brick ghg){
        this.brick=ghg;
        this.pos=new Pos(brick.pos.x,brick.pos.y);
       this.radiuschange=radiuschange;

    };

    public void hitaction(balls bs){
        brick.hitaction(bs);
        for(ball b:bs){
            float dx=brick.xLength;
            float dy=brick.yLength;
            if (b.pos.Eculideandistance(this.pos)<game.redundency+Math.sqrt(dx*dx+dy*dy)+b.radius)
            {b.sizeMul(radiuschange);}
        }
    };


    @Override
    public boolean hitted(balls bs) {
        return brick.hitted( bs);
    }

    @Override
    public void draw(Canvas c, Paint p) {
        this.brick.draw( c,p);
    }


}
