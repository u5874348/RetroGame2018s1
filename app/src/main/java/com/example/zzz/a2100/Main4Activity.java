package com.example.zzz.a2100;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;





public class Main4Activity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {
     static int life;
    static int difficult;
private static final String filename="peizhi";
    TextView t5;
    TextView t7;
       private SharedPreferences share;
       private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        t5=findViewById(R.id.textView5);
        t7=findViewById(R.id.textView7);
        share=this.getSharedPreferences(filename,Activity.MODE_PRIVATE);//load from the file we saved.
        editor=share.edit();





         //load the settings.
        this.difficult=share.getInt("diff",3);//if the users first changes the settings, the default is 3.
        this.life=share.getInt("life",3);







        SeekBar s= findViewById(R.id.seekBar2);

        t7.setText(difficult+"");//show the difficulty

        s.setProgress(difficult-1);
        s.setOnSeekBarChangeListener(this);


        SeekBar s1= findViewById(R.id.seekBar3);

        s1.setOnSeekBarChangeListener(this);
        s1.setProgress(life-1);
        t5.setText(life+"");

        Button b=findViewById(R.id.button5);
        b.setOnClickListener(this);




    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {//use seekbar to change the game settings.
         if (seekBar.getId()==R.id.seekBar2)
        {
            difficult=progress+1;//the seek bar is from 0-4,so I show the diffculty from 1-5. the life is the same.
            editor.putInt("diff",difficult);//save the settings.
            editor.commit();//commit.



            t7.setText(difficult+"");}
        else if (seekBar.getId()==R.id.seekBar3)
        {
            life=progress+1;
            editor.putInt("life",life);
            editor.commit();

            t5.setText(life+"");


        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class); //back to main menu
        startActivity(intent);
    }
}
