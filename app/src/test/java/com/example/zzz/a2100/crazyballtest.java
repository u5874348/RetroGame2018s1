package com.example.zzz.a2100;

import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Created by u5925324 on 17/05/18.
 */

public class crazyballtest {
    game g1=new game(5);
    game g2=new game(1);
    @Test
    public void difficultytest(){ //test whether correct number of brisks are added.

        assertEquals(5,g2.rows_of_bricks);
        assertTrue("bricks not correct",g1.rows_of_bricks==17);

    }
    @Test
    public void ballnumber(){
        assertEquals(2,g2.bs.size());//at the start of the game  there are 2 balls.
    }

    @Test
    public void brisknumbertest(){//test whether colorful brisks are in suitable range.
        int simple=0;
        int color=0;
        for (int i=0;i<=100;i++){
            game g=new game(5);
            Bricks b=g.bricks;

            for (Brick brick:b){
                if (brick instanceof SimpleBrick) simple+=1; //test how many simple brisk are generate.
                else color+=1;
            }
        }

       //we decide to generate 2n simple one and n complex one.  It generates by random, so 100 times avoid much error by random.

        assertTrue("simplebrisks are:"+simple+" colorbrisks are:"+color+" so the number is wrong",Math.abs(simple-2*color)<5*100);
              // one time the error by random is less than 5.100 times is less than 500. That means simple roughly equals to 2*color.

    }

}
